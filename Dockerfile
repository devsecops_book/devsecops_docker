FROM debian:stretch

RUN apt-get update && apt-get install -y --no-install-recommends \
	curl=7.52.1-5+deb9u12 \
	nginx=1.10.3-1+deb9u5 \
	git=1:2.11.0-3+deb9u7 \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN addgroup user
RUN useradd -rm -d /home/user -s /bin/bash -g user -u 1001 user

CMD ["sleep", "infinity"]

